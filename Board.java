/**
 * Created by gorax on 1/18/2016.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

public class Board extends JComponent
{
    // dimension of checkerboard square (25% bigger than checker)
    private final static int SQUAREDIM = (int) (Checker.getDimension() * 1.25);

    //
    private final int TILECOUNT = 8;

    // dimension of checkerboard (width of 8 squares)
    private final int BOARDDIM = TILECOUNT * SQUAREDIM;

    // preferred size of Board component
    private Dimension dimPrefSize;

    // dragging flag -- set to true when user presses mouse button over checker
    // and cleared to false when user releases mouse button
    private boolean inDrag = false;

    // displacement between drag start coordinates and checker center coordinates
    private int deltax, deltay;

    // reference to positioned checker at start of drag
    private PosCheck posCheck;

    // center location of checker at start of drag
    private int oldcx, oldcy;

    // list of Checker objects and their initial positions
    private List<PosCheck> posChecks;

    // old grid piece number
    private int oldxpos, oldypos;

    //new grid piece number
    private int newxpos, newypos;

    public Board()
    {
        posChecks = new ArrayList<>();
        dimPrefSize = new Dimension(BOARDDIM, BOARDDIM);

        final AllowedMoves am = new AllowedMoves();

        addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent me)
            {
                // Obtain mouse coordinates at time of press.
                int x = me.getX();
                int y = me.getY();

                // Locate positioned checker under mouse press.

                for (PosCheck posCheck: posChecks)
                    if (Checker.contains(x, y, posCheck.cx,
                            posCheck.cy))
                    {
                        Board.this.posCheck = posCheck;
                        oldxpos = posCheck.xpos;
                        oldypos = posCheck.ypos;
                        oldcx = posCheck.cx;
                        oldcy = posCheck.cy;
                        deltax = x - posCheck.cx;
                        deltay = y - posCheck.cy;
                        inDrag = true;
                        System.out.println("the clicked on piece position x-> " + oldxpos + " y => " + oldypos);
                        return;
                    }


            }

            @Override
            public void mouseReleased(MouseEvent me)
            {
                // When mouse released, clear inDrag (to
                // indicate no drag in progress) if inDrag is
                // already set.

                if (inDrag)
                    inDrag = false;
                else
                    return;

                // Snap checker to center of square.
                int x = me.getX();
                int y = me.getY();
                posCheck.cx = (x - deltax) / SQUAREDIM * SQUAREDIM +
                        SQUAREDIM / 2;
                posCheck.cy = (y - deltay) / SQUAREDIM * SQUAREDIM +
                        SQUAREDIM / 2;

                //set the x and y on the 8 x 8 grid
                newxpos = (int) Math.ceil(posCheck.cx / SQUAREDIM) + 1;
                newypos = (int) Math.ceil(posCheck.cy / SQUAREDIM) + 1;
                System.out.println("mapped x " + posCheck.cx + " to " + newxpos);
                System.out.println("mapped y " + posCheck.cy + " to " + newypos);
                System.out.println("the released piece position x-> " + newxpos + " y => " + newypos);
                Board.this.posCheck.xpos = newxpos;
                Board.this.posCheck.ypos = newypos;

                //move to old position if this is an illegal move
                if(!am.isValidNonJumpMove(oldxpos, oldypos, newxpos, newypos, Board.this.posCheck.checker))
                {
                    Board.this.posCheck.cx = oldcx;
                    Board.this.posCheck.cy = oldcy;
                    //return the x and y on the 8 x 8 grid to the original x, y
                    Board.this.posCheck.xpos = oldxpos;
                    Board.this.posCheck.ypos = oldypos;
                }


                //make sure we are not on an occupied square.
                for (PosCheck posCheck: posChecks)
                {

                    if (posCheck != Board.this.posCheck &&
                            posCheck.cx == Board.this.posCheck.cx &&
                            posCheck.cy == Board.this.posCheck.cy
                            )
                    {
                        Board.this.posCheck.cx = oldcx;
                        Board.this.posCheck.cy = oldcy;
                        //return the x and y on the 8 x 8 grid to the original x, y
                        Board.this.posCheck.xpos = oldxpos;
                        Board.this.posCheck.ypos = oldypos;
                    }
                }

                //black starts at either 7 or 8 and has to get to row 1 to become a king
                if(Board.this.posCheck.checker.getCheckerType().equals("BLACK_REGULAR")){
                    if(Board.this.posCheck.ypos == 1)
                        Board.this.posCheck.checker.setKing(CheckerType.BLACK_KING);
                }

                //reds start at row 1 and must get to row 8 to become a king
                if(Board.this.posCheck.checker.getCheckerType().equals("RED_REGULAR")){
                    if(Board.this.posCheck.ypos == 8)
                        Board.this.posCheck.checker.setKing(CheckerType.RED_KING);
                }



                posCheck = null;
                repaint();


            }
        });

        // Attach a mouse motion listener to the applet. That listener listens
        // for mouse drag events.

        addMouseMotionListener(new MouseMotionAdapter()
        {
            @Override
            public void mouseDragged(MouseEvent me)
            {
                if (inDrag)
                {
                    // Update location of checker center.
                    posCheck.cx = me.getX() - deltax;
                    posCheck.cy = me.getY() - deltay;
                    repaint();
                }
            }
        });

    }


    public void add(Checker checker, int row, int col)
    {
        if (row < 1 || row > 8)
            throw new IllegalArgumentException("row out of range: " + row);
        if (col < 1 || col > 8)
            throw new IllegalArgumentException("col out of range: " + col);
        PosCheck posCheck = new PosCheck();
        posCheck.checker = checker;

        posCheck.cx = (col - 1) * SQUAREDIM + SQUAREDIM / 2;
        posCheck.cy = (row - 1) * SQUAREDIM + SQUAREDIM / 2;

        posCheck.xpos = col;
        posCheck.ypos = row;

        System.out.println("added checker to position x " + posCheck.cx + " and y " + posCheck.cy);
        System.out.println("added checker to square x " + posCheck.xpos + " and y " + posCheck.ypos);
        for (PosCheck _posCheck: posChecks)
            if (posCheck.cx == _posCheck.cx && posCheck.cy == _posCheck.cy)
                throw new AlreadyOccupiedException("square at (" + row + "," +
                        col + ") is occupied");
        posChecks.add(posCheck);
    }



    @Override
    public Dimension getPreferredSize()
    {
        return dimPrefSize;
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        paintCheckerBoard(g);
        for (PosCheck posCheck: posChecks)
            if (posCheck != Board.this.posCheck)
                posCheck.checker.draw(g, posCheck.cx, posCheck.cy);

        // Draw dragged checker last so that it appears over any underlying
        // checker.

        if (posCheck != null)
            posCheck.checker.draw(g, posCheck.cx, posCheck.cy);
    }

    private void paintCheckerBoard(Graphics g)
    {
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        // Paint checkerboard.

        for (int row = 0; row < 8; row++)
        {
            g.setColor(((row & 1) != 0) ? Color.BLACK : Color.WHITE);
            for (int col = 0; col < 8; col++)
            {
                g.fillRect(col * SQUAREDIM, row * SQUAREDIM, SQUAREDIM, SQUAREDIM);
                g.setColor((g.getColor() == Color.BLACK) ? Color.WHITE : Color.BLACK);
            }
        }
    }



    // positioned checker helper class

    private class PosCheck
    {
        public Checker checker;
        //actual x and y position center of checker piece
        public int cx;
        public int cy;
        //position on the 8 x 8 board
        //makes the math easier
        public int xpos;
        public int ypos;

    }
}