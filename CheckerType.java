/**
 * Created by gorax on 1/18/2016.
 */
public enum CheckerType
{
    BLACK_REGULAR,
    BLACK_KING,
    RED_REGULAR,
    RED_KING
}