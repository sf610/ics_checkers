/**
 * Created by gorax on 1/18/2016.
 */
public class AlreadyOccupiedException extends RuntimeException
{
    public AlreadyOccupiedException(String msg)
    {
        super(msg);
    }
}
