/**
 * Created by gorax on 1/18/2016.
 */
import java.awt.EventQueue;

import javax.swing.JFrame;

public class Checkers extends JFrame
{
    public Checkers(String title)
    {
        super(title);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        Board board = new Board();

        for (int row=1; row< 9; row++){
            for (int col = 1; col < 9; col++){

                if (row < 4 && (((col + row) % 2) == 1)){
                    board.add(new Checker(CheckerType.BLACK_REGULAR), row, col);
                }

                if (row >5 && (((col + row) % 2) == 1)){
                    board.add(new Checker(CheckerType.RED_REGULAR),row, col);
                }
            }
        }

        setContentPane(board);

        pack();
        setVisible(true);
    }

    public static void main(String[] args)
    {
        Runnable r = new Runnable()
        {
            @Override
            public void run()
            {
                new Checkers("Checkers");
            }
        };
        EventQueue.invokeLater(r);
    }
}