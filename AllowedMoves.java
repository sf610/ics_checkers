/**
 * Created by gorax on 1/19/16.
 */
public class AllowedMoves {

    public AllowedMoves()
    {

    }




    public boolean isValidJump(int oldx, int oldy, int newx, int newy)
    {
        return false;
    }



    public boolean isValidNonJumpMove(int oldx, int oldy, int newx, int newy, Checker checker)
    {
        String type = checker.getCheckerType();
        //distances must be equal for a move to be valid
        int allowedmaxx = oldx+1;
        int allowedminx = oldx-1;
        int allowedmaxy = oldy+1;
        int allowedminy = oldy-1;

        System.out.println("in allowed moves with oldx => " + oldx + " old y => " + oldy);
        System.out.println("in allowed moves with newx => " + newx + " new y => " + newy);

        if(checker.getCheckerType().equals("RED_REGULAR"))
        {
            System.out.println("checking move for red regular");
            return ((newy == allowedminy) && (newx == allowedmaxx || newx == allowedminx) );
        }
        if(checker.getCheckerType().equals("BLACK_REGULAR"))
        {
            System.out.println("checking move for black regular");
            return ((newy == allowedmaxy) && (newx == allowedmaxx || newx == allowedminx) );
        }

        System.out.println("checking move for kinged piece");
        return ((newx == allowedminx || newx == allowedmaxx) && (newy == allowedmaxy || newy == allowedminy));


    }
}
